#Import Lines
from finch import Finch
from time import sleep
import RPi.GPIO as GPIO
import sys

#GPIO Setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

#Finch Stuff Function
def makeFinchGo():
	# Instantiate the Finch object
	myFinch = Finch()

	# Actually Doing Stuff
	myFinch.led(255,255,0)
	myFinch.wheels(.5,.5)
	sleep(1)

	myFinch.led(0,255,255)
	myFinch.wheels(-.5,-.5)
	sleep(1)

	myFinch.led(255,0,255)
	myFinch.wheels(.25,.1)
	sleep(2)

	myFinch.led(255,0,255)
	myFinch.wheels(1,.5)
	sleep(2)

	#Stop Motors
	myFinch.halt()

	#Close Program
	myFinch.close()

#Button Push
while True:
        if GPIO.input(3) == GPIO.LOW:
                print("The button was pushed.")
		makeFinchGo()
                #GPIO.cleanup()
                #sys.exit()

