# A simple Finch dance in Python

from finch import Finch
from time import sleep

print("New Dance!")
# Instantiate the Finch object
snakyFinch = Finch()
    

# Do a six step dance
snakyFinch.led(0,0,255)
snakyFinch.wheels(-0.5,0.5)
sleep(0.5)

snakyFinch.led(0,255,0)
snakyFinch.wheels(-.05,0.5)
sleep(0.5)

snakyFinch.led(0,0,255)
snakyFinch.wheels(-1,1)
sleep(0.5)

snakyFinch.led(255,0,0)
snakyFinch.wheels(-1,-1)
sleep(0.5)

snakyFinch.led(0,255,255)
snakyFinch.wheels(.5,-0.5)
sleep(1)

snakyFinch.led(255,255,255)
snakyFinch.wheels(0.5,-0.5)
sleep(1)

snakyFinch.close();

